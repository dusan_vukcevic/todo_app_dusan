var express = require ('express');
var app = express();
var mongojs = require('mongojs');
var db= mongojs('userlist',['userlist', 'contactlist']);
var bodyParser= require('body-parser');


app.use(express.static(__dirname + "/public1"));
app.use(bodyParser.json());




app.get('/userlist', function (req, res){
	
	db.userlist.find(function(err, docs){
		
		res.json(docs);
	});
	
});

app.get('/contactlist/:username', function (req, res){
	
	var x = req.params.username;
	if (x.length==24) {
		db.contactlist.findOne({_id: mongojs.ObjectId(x)}, function(err,doc){
		res.json(doc);
	});
	}

		else{
	db.contactlist.find({user: x},function(err, docs){
		
		res.json(docs);
	});

	}
});


app.post('/contactlist', function(req, res){
	
	db.contactlist.insert(req.body, function(err, doc){
		res.json(doc);
	});
});

app.post('/userlist', function(req, res){
	
	db.userlist.insert(req.body, function(err, doc){
		console.log(req.body);
		res.json(doc);
	});
});


app.delete('/contactlist/:id', function(req, res){
	var id = req.params.id;
	
	db.contactlist.remove({_id: mongojs.ObjectId(id)}, function(err,doc){
		res.json(doc);
	});
});

app.get('/contactlist', function (req, res){
	

	db.contactlist.find(function(err, docs){
		
		res.json(docs);
	});
	
});

app.get('/checkPassword/:username/:password', function (req, res)
{
		var username= req.params.username;
		var password = req.params.password;
		
		db.userlist.findOne({username: username},function(err, docs){
			
			if (docs!=null)
			{
			if (password == docs.password)
				 res.json(docs); else {
				 	res.json('no');
				 }
			} else {
				res.json('no');
			}

	});

});

app.get('/userlist/:username', function (req, res){
	
	
	var username= req.params.username;
	
	db.userlist.findOne({username: username},function(err, docs){
		
		res.json(docs);
	});
	
});

app.put('/contactlist/:id', function(req, res){
	var id = req.params.id;
	
	db.contactlist.findAndModify({query: {_id: mongojs.ObjectId(id)},
		update: {$set: {name: req.body.name, tag: req.body.tag, user:req.body.user,date: req.body.date, completed: req.body.completed }},
		new: true}, function(err, doc){
			res.json(doc);
		}

)
});



app.listen(3000);
console.log("server running on port 3000");